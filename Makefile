NAME			=	libfts.a
rwildcard		=	$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2)\
					$(filter $(subst *,%,$2),$d))

COMPILER		=	nasm
CFLAGS			=	-f macho64

OBJ_DIR			=	objs
SRCS			=	$(call rwildcard, ./, *.s)
OBJS			=	$(addprefix $(OBJ_DIR)/, $(SRCS:.s=.o))

OBJ_SUB_DIRS	=	$(dir $(OBJS))

all: moulitest $(NAME)

moulitest:
	@git clone https://github.com/yyang42/moulitest.git

$(NAME): $(OBJS)
	@echo "linking $@"
	@ar rc $@ $^
	@ranlib $@

$(OBJS): | $(OBJ_DIR)

$(OBJ_DIR):
	@$(foreach dir, $(OBJ_SUB_DIRS), mkdir -p $(dir);)

$(OBJ_DIR)/%.o: %.s
	@echo "compiling $(notdir $<)"
	@$(COMPILER) $(CFLAGS) $^ -o $@

clean:
	@echo "cleaning objects"
	@rm -rf $(OBJ_DIR)

fclean: clean
	@echo "cleaning $(NAME)"
	@rm -f $(NAME)
	@echo "cleaning test binary"
	@rm -f a.out

re:
	@$(MAKE) fclean
	@$(MAKE) all
