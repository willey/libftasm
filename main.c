/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wburgos <wburgos@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/21 18:58:36 by wburgos           #+#    #+#             */
/*   Updated: 2015/05/30 16:50:15 by wburgos          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include "libfts.h"

void	write_bytes(char *str, int n)
{
	write(1, "[", 1);
	for (int i = 0; i < n; i++)
	{
		if (str[i] == 0)
			write(1, " 0 ", 3);
		else
		{
			write(1, " '", 2);
			write(1, str + i, 1);
			write(1, "' ", 2);
		}
	}
	write(1, "]\n", 2);
}

void test_bzero()
{
	char	*str = strdup("Hello");
	char	*str2 = strdup("olleH");

	write(1, "Initial string: ", 16);
	write_bytes(str, 5);
	bzero(str, strlen(str));
	write(1, "After bzero:    ", 16);
	write_bytes(str, 5);

	write(1, "Initial string: ", 16);
	write_bytes(str2, 5);
	ft_bzero(str2, strlen(str2));
	write(1, "After ft_bzero: ", 16);
	write_bytes(str2, 5);

	printf("Comparing data between result strings ... \n");
	if (memcmp(str, str2, 5) == 0)
		printf("OK\n");
	else
		printf("Fail :(\n");

	free(str);
	free(str2);
}

void test_is(int (*ft)(int), int (*libc)(int))
{
	int success = 1;

	printf("Testing various ints ... \n");
	for (int i = -200; i < 201; i++)
	{
		if (ft(i) != libc(i))
		{
			printf("FAILED on int %d: libc returned '%d', ft returned '%d'\n", i, libc(i), ft(i));
			success = 0;
		}
	}
	if (success)
		printf("All good !\n");
}

void test_memset()
{
	char	*str = strdup("Hello");
	char	*str2 = strdup("olleH");

	write(1, "Initial string: ", 16);
	write_bytes(str, 5);
	memset(str, 'a', strlen(str));
	write(1, "After memset:   ", 16);
	write_bytes(str, 5);

	write(1, "Initial string: ", 16);
	write_bytes(str2, 5);
	ft_memset(str2, 'a', strlen(str2));
	write(1, "After ft_memset:", 16);
	write_bytes(str2, 5);

	printf("Comparing data between result strings ... \n");
	if (memcmp(str, str2, 5) == 0)
		printf("OK\n");
	else
		printf("Fail :(\n");

	free(str);
	free(str2);
}

void test_memcpy()
{
	char	*str = strdup("Hello");
	char	*str2 = strdup("olleH");
	char	*cpy = strdup("Copy!");

	write(1, "Initial string: ", 16);
	write_bytes(str, 5);
	memcpy(str, cpy, strlen(str));
	write(1, "After memcpy:   ", 16);
	write_bytes(str, 5);

	write(1, "Initial string: ", 16);
	write_bytes(str2, 5);
	ft_memcpy(str2, cpy, strlen(str2));
	write(1, "After ft_memcpy:", 16);
	write_bytes(str2, 5);

	printf("Comparing data between result strings ... \n");
	if (memcmp(str, str2, 5) == 0)
		printf("OK\n");
	else
		printf("Fail :(\n");

	free(str);
	free(str2);
	free(cpy);
}

void test_strncpy()
{
	char	*str = strdup("Hello");
	char	*str2 = strdup("olleH");
	char	*cpy = strdup("Copy!");

	write(1, "Initial string: ", 16);
	write_bytes(str, 5);
	strncpy(str, cpy, strlen(str));
	write(1, "After strncpy:   ", 16);
	write_bytes(str, 5);

	write(1, "Initial string: ", 16);
	write_bytes(str2, 5);
	ft_strncpy(str2, cpy, strlen(str2));
	write(1, "After ft_strncpy:", 16);
	write_bytes(str2, 5);

	printf("Comparing data between result strings ... \n");
	if (memcmp(str, str2, 5) == 0)
		printf("OK\n");
	else
		printf("Fail :(\n");

	free(str);
	free(str2);
	free(cpy);
}

void test_strcpy()
{
	char	*str = strdup("Hello");
	char	*str2 = strdup("olleH");
	char	*cpy = strdup("Copy!");

	write(1, "Initial string: ", 16);
	write_bytes(str, 5);
	strcpy(str, cpy);
	write(1, "After strcpy:   ", 16);
	write_bytes(str, 5);

	write(1, "Initial string: ", 16);
	write_bytes(str2, 5);
	ft_strcpy(str2, cpy);
	write(1, "After ft_strcpy:", 16);
	write_bytes(str2, 5);

	printf("Comparing data between result strings ... \n");
	if (memcmp(str, str2, 5) == 0)
		printf("OK\n");
	else
		printf("Fail :(\n");

	free(str);
	free(str2);
	free(cpy);
}

void test_strcat()
{
	char	str[32] = { 0 };
	char	str2[32] = { 0 };

	strcpy(str, "Salut");
	strcpy(str2, "Salut");

	write(1, "Initial string: ", 16);
	printf("\"%s\"\n", str);
	write(1, "After strcat:   ", 16);
	strcat(str, " les potes");
	printf("\"%s\"\n", str);

	write(1, "Initial string: ", 16);
	printf("\"%s\"\n", str2);
	write(1, "After ft_strcat:", 16);
	ft_strcat(str2, " les potes");
	printf("\"%s\"\n", str2);

	printf("Comparing data between result strings ... \n");
	if (memcmp(str, str2, 5) == 0)
		printf("OK\n");
	else
		printf("Fail :(\n");
}

int main(int ac, char **av)
{
	int fd = 0;

	printf("****** TEST FT_BZERO ******\n");
	test_bzero();

	printf("\n****** TEST FT_STRCAT ******\n");
	test_strcat();

	printf("\n****** TEST FT_ISASCII ******\n");
	test_is(&ft_isascii, &isascii);

	printf("\n****** TEST FT_ISDIGIT ******\n");
	test_is(&ft_isdigit, &isdigit);

	printf("\n****** TEST FT_ISALPHA ******\n");
	test_is(&ft_isalpha, &isalpha);

	printf("\n****** TEST FT_ISALNUM ******\n");
	test_is(&ft_isalnum, &isalnum);

	printf("\n****** TEST FT_ISPRINT ******\n");
	test_is(&ft_isprint, &isprint);

	printf("\n****** TEST FT_TOLOWER ******\n");
	test_is(&ft_tolower, &tolower);

	printf("\n****** TEST FT_TOUPPER ******\n");
	test_is(&ft_toupper, &toupper);

	printf("\n****** TEST PUTS ******\n");
	ft_puts("Salut");
	ft_puts(NULL);

	printf("\n****** TEST FT_MEMSET ******\n");
	test_memset();

	printf("\n****** TEST FT_MEMCPY ******\n");
	test_memcpy();

	printf("\n****** TEST FT_ISLOWER ******\n");
	test_is(&ft_islower, &islower);

	printf("\n****** TEST FT_ISUPPER ******\n");
	test_is(&ft_isupper, &isupper);

	printf("\n****** TEST FT_ISSPACE ******\n");
	test_is(&ft_isspace, &isspace);

	printf("\n****** TEST FT_ISBLANK ******\n");
	test_is(&ft_isblank, &isblank);

	printf("\n****** TEST FT_STRCPY ******\n");
	test_strcpy();

	printf("\n****** TEST FT_STRNCPY ******\n");
	test_strncpy();

	printf("\n****** TEST FT_PUTSTR ******\n");
	ft_putstr("It works !");
	ft_putstr(NULL);

	printf("\n\n****** TEST FT_CAT ******\n");
	if (ac == 2)
		fd = open(av[1], O_RDONLY);
	ft_cat(fd);
	ft_cat(-42);
	return (0);
}
