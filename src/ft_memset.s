section .text
	global _ft_memset

_ft_memset:
	push rdi
	mov rax, rsi
	mov rcx, rdx
	cld				; clear direction flag
	rep stosb		; store byte to string (al vers rdi)

end:
	pop rax
	ret