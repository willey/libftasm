%define MACH_SYSCALL(nb)	0x2000000 | nb
%define STDIN				0
%define STDOUT				1
%define READ				3
%define WRITE				4

section .text
	global _ft_cat
	extern _ft_putstr

_ft_cat:
	push rax
	push rdi
	lea rsi, [rel buf]

loop:
	mov rdx, 4
	mov rax, MACH_SYSCALL(READ)
	syscall
	jc error

	cmp rax, 0 ; end of read
	jz end

	mov byte[rsi+rax], 0 ; Terminate current buffer with 0

	push rdi
	mov rdi, rsi
	call _ft_putstr
	pop rdi
	jmp loop

error:
	mov rax, 1

end:
	pop rdi
	pop rax
	ret

section .bss
	buf resb 4