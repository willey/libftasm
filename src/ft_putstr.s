%define STDOUT				1
%define MAC_SYSCALL(nb)		0x2000000 | nb
%define WRITE				4

section .text
	global _ft_putstr
	extern _ft_strlen

_ft_putstr:
	cmp rdi, 0
	jz end

	push rdi
	call _ft_strlen
	push rax
	lea rsi, [rel rdi]
	mov rdi, STDOUT
	mov rdx, rax
	mov rax, MAC_SYSCALL(WRITE)
	syscall

	pop rax
	pop rdi

end:
	ret