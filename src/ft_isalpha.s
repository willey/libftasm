section .text
	global _ft_isalpha

_ft_isalpha:
	cmp rdi, 'A'
	jl false
	cmp rdi, 'Z'
	jle true
	cmp rdi, 'z'
	jg false
	cmp rdi, 'a'
	jge true

false:
	mov rax, 0
	jmp end

true:
	mov rax, 1

end:
	ret