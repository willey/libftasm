section .text
	global _ft_bzero

_ft_bzero:
	mov rcx, rdi
	mov rdx, rsi

loop:
	cmp rdx, 0
	jle end
	mov byte[rcx], 0
	inc rcx
	dec rdx
	jmp loop

end:
	ret