section .text
	global _ft_memcpy

_ft_memcpy:
	push rdi
	mov rcx, rdx
	cld				; clear direction flag
	rep movsb		; Move [rsi] vers [rdi]

end:
	pop rax
	ret