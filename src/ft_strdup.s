section .text
	global _ft_strdup
	extern _malloc
	extern _ft_memcpy
	extern _ft_strlen

_ft_strdup:
	push rdi
	call _ft_strlen
	mov rdx, rax
	mov rdi, rax
	call _malloc
	cmp rax, 0
	jz end
	mov rdi, rax
	pop rsi
	call _ft_memcpy

end:
	ret
