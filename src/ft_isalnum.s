section .text
	global _ft_isalnum

_ft_isalnum:
	mov rdx, rdi
	cmp rdx, '0'
	jl false
	cmp rdx, '9'
	jle true
	cmp rdx, 'A'
	jl false
	cmp rdx, 'Z'
	jle true
	cmp rdx, 'z'
	jg false
	cmp rdx, 'a'
	jge true

false:
	mov rax, 0
	jmp end

true:
	mov rax, 1

end:
	ret
