section .text
	global _ft_isprint

_ft_isprint:
	mov rdx, rdi
	cmp rdx, 32
	jl false
	cmp rdx, 126
	jg false
	jmp true

false:
	mov rax, 0
	jmp end

true:
	mov rax, 1

end:
	ret
