section .text
	global _ft_strncpy

_ft_strncpy:
	push rdi
	mov rcx, rdx
	cld				; clear direction flag
	rep movsb		; Move [rsi] vers [rdi]

end:
	pop rax
	ret