section .text
	global _ft_isblank

_ft_isblank:
	cmp rdi, 9
	je true
	cmp rdi, ' '
	je true
	mov rax, 0
	jmp end

true:
	mov rax, 1

end:
	ret
