section .text
	global _ft_strcpy
	extern _ft_strlen
	
_ft_strcpy:
	push rdi
	mov rdi, rsi
	call _ft_strlen
	mov rcx, rax
	pop rdi
	mov rax, rdi
	cld				; clear direction flag
	rep movsb		; Move byte from string to string (rsi vers rdi)
	ret