section .text
	global _ft_isspace

_ft_isspace:
	cmp rdi, 9
	je true
	cmp rdi, 10
	je true
	cmp rdi, 11
	je true
	cmp rdi, 12
	je true
	cmp rdi, 13
	je true
	cmp rdi, ' '
	je true
	mov rax, 0
	jmp end

true:
	mov rax, 1

end:
	ret
