section .text
	global _ft_isupper

_ft_isupper:
	cmp rdi, 'A'
	jl false
	cmp rdi, 'Z'
	jg false
	jmp true

false:
	mov rax, 0
	jmp end

true:
	mov rax, 1

end:
	ret
