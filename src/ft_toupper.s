section .text
	global _ft_toupper

_ft_toupper:
	mov rax, rdi
	cmp rax, 'a'
	jl end
	cmp rax, 'z'
	jg end
	sub rax, 32

end:
	ret
