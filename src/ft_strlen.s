section .text
	global _ft_strlen

_ft_strlen:
	push rdi
	sub rcx, rcx
	not	rcx
	sub	al, al
	cld
	repne scasb
	not	rcx
	pop	rdi
	lea	rax, [rcx-1]
	ret