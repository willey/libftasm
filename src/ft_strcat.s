section .text
	global _ft_strcat
	extern _ft_strlen

_ft_strcat:
	push rdi

loop_s1:
	cmp byte[rdi], 0
	jz copy_str
	inc rdi
	jmp loop_s1

copy_str:
	push rdi
	mov rdi, rsi
	call _ft_strlen
	mov rcx, rax
	pop rdi
	cld				; clear direction flag
	rep movsb		; Move [rsi] vers [rdi]

end:
	pop rax
	ret