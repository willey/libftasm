section .text
	global _ft_isascii

_ft_isascii:
	mov rdx, rdi
	cmp rdx, 0
	jl false
	cmp rdx, 127
	jg false
	jmp true

false:
	mov rax, 0
	jmp end

true:
	mov rax, 1

end:
	ret
