section .text
	global _ft_islower

_ft_islower:
	cmp rdi, 'a'
	jl false
	cmp rdi, 'z'
	jg false
	jmp true

false:
	mov rax, 0
	jmp end

true:
	mov rax, 1

end:
	ret
