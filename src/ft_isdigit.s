section .text
	global _ft_isdigit

_ft_isdigit:
	mov rdx, rdi
	cmp rdx, '0'
	jl false
	cmp rdx, '9'
	jg false
	jmp true

false:
	mov rax, 0
	jmp end

true:
	mov rax, 1

end:
	ret
