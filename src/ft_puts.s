%define STDOUT				1
%define MAC_SYSCALL(nb)		0x2000000 | nb
%define WRITE				4

section .data
null_str:
	.msg db "(null)", 10
	.len equ $ - null_str.msg

section .text
	global _ft_puts
	extern _ft_strlen

eol:
	.char db 10

_ft_puts:
	push rdi
	cmp rdi, 0
	jz is_null

	call _ft_strlen
	push rax
	lea rsi, [rel rdi]
	mov rdi, STDOUT
	mov rdx, rax
	mov rax, MAC_SYSCALL(WRITE)
	syscall
	jc error

	mov rdx, 1
	lea rsi, [rel eol.char]
	mov rax, MAC_SYSCALL(WRITE)
	syscall
	jc error
	
	pop rax
	jmp end

is_null:
	mov rdi, STDOUT
	mov rdx, null_str.len
	lea rsi, [rel null_str.msg]
	mov rax, MAC_SYSCALL(WRITE)
	syscall
	jc error
	mov rax, null_str.len
	jmp end

error:
	mov rax, -1

end:
	pop rdi
	ret