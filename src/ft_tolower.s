section .text
	global _ft_tolower

_ft_tolower:
	mov rax, rdi
	cmp rax, 'A'
	jl end
	cmp rax, 'Z'
	jg end
	add rax, 32

end:
	ret
